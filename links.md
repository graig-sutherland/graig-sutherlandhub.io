---
layout: page
title: Links
permalink: /links/
order: 3
---

* [yr.no](http://yr.no): weather forecasts provided by the Norwegian Meteorological Institute
* [met.no](http://met.no): homepage of the Norwegian Meteorological Institute
* [Copernicus](http://marine.copernicus.eu): European marine environment monitoring service
* [Norwegian Ice Service](http://polarview.met.no): ice forecasts and climatology provided by MET Norway

---
layout: page
title: About
permalink: /about/
order: 5
---

I am a physical oceanographer living in Oslo with my [wife](http://jessicabrouder.org) and two children. My journey to Oslo started in Victoria BC, where I was born, with stops in Montr&#x00E9;al, St. John's, Berlin and Galway.

![Me](/images/IMG_4501.jpg)

<!-- This is the base Jekyll theme. You can find out more info about customizing your Jekyll theme, as well as basic Jekyll usage documentation at [jekyllrb.com](http://jekyllrb.com/) -->

<!-- You can find the source code for the Jekyll new theme at: -->
<!-- {% include icon-github.html username="jekyll" %} / -->
<!-- [minima](https://github.com/jekyll/minima) -->

<!-- You can find the source code for Jekyll at -->
<!-- {% include icon-github.html username="jekyll" %} / -->
<!-- [jekyll](https://github.com/jekyll/jekyll) -->
